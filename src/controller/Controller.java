package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
	public static int getMin(IntegersBag bag) {
		return model.getMin(bag);
	}
	
	public static  int multiplicacionV ( IntegersBag bag){
		return model.multiplicacionV(bag);
	}
	
	public static int numerosPares(IntegersBag bag ){
		return model.numerosPares(bag);
	}
	
}
