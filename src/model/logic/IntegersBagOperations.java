package model.logic;

import java.util.Iterator;

import javafx.util.converter.NumberStringConverter;
import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin(IntegersBag bag){
		int min = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				
				
				value = iter.next();
				
				if ( min == Integer.MIN_VALUE)
				{
					min = value;
				}
				if(min > value){
					min = value;
				}
			}
			
		}
		return min;
	}

	
	public int multiplicacionV ( IntegersBag bag)
	{

		int rta = -1;
		int value ;
		if ( bag != null)
		{
			Iterator <Integer> iter = bag.getIterator();
			while (iter.hasNext())
			{
				value = iter.next();
				
				if (rta == -1){
					rta = value;
				}
				else {
					rta = rta * value;
				}
				
			}
		}
	return rta;
	}
	
	public int numerosPares(IntegersBag bag )
	{
		int rta = 0;
		int value ;
		if  ( bag != null)
		{
			
			Iterator <Integer> iter = bag.getIterator();
			while (iter.hasNext())
			{ 
				value = iter.next();
				if ( value % 2 == 0)
				{
					rta ++;
				}
			}
		}
		return rta;
	}
	
	
}
